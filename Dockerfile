# syntax=docker/dockerfile:1
ARG TARGETOS
ARG TARGETARCH
ARG GOLANG_VERSION=1.21.11

# 准备阶段: 通过 git 下载 MinIO 源码打上本地定制的补丁
FROM docker.io/alpine/git:2.45.2 AS apply_source_code_patches
# 以patch补丁形式对官方源码进行二次开发
COPY patches/*.patch /patches/
WORKDIR /src
RUN MINIO_GIT_REPO="https://gitee.com/mirrors/minio.git" && \
    MINIO_RELEASE_TAG="RELEASE.2024-06-26T01-06-18Z" && \
    git clone --quiet --depth=1 -b $MINIO_RELEASE_TAG $MINIO_GIT_REPO minio \
    && \
    cd minio \
    && \
    find /patches -maxdepth 1 -type f -name "*.patch" -print | \
        xargs git apply \
    && \
    cd .. \
    && \
    tar czf /src/minio-src.tar.gz --exclude=minio/.git minio && ls -hlF /src

# 编译阶段
FROM docker.io/library/golang:${GOLANG_VERSION}-alpine AS builder

ENV GOPATH=/go \
    GO111MODULE=on \
    GOPROXY=https://goproxy.cn,direct

WORKDIR /go/src
COPY --from=apply_source_code_patches /src/minio-src.tar.gz /go/src/
RUN tar xzf minio-src.tar.gz \
    && \
    cd minio \
    && \
    CGO_ENABLED=0 GOOS=${TARGETOS} GOARCH=${TARGETARCH} \
        go build .

# 基于已发布的原始镜像(借用其中除minio主程序之外的若干配置文件及可执行程序)
FROM docker.io/minio/minio:RELEASE.2024-06-26T01-06-18Z AS original_image

# 最终镜像
FROM registry.access.redhat.com/ubi9/ubi-micro:latest

ARG RELEASE

LABEL name="MinIO" \
      vendor="Liu Qun <517067180@qq.com>" \
      maintainer="Liu Qun <517067180@qq.com>" \
      version="${RELEASE}" \
      release="${RELEASE}" \
      summary="MinIO is a High Performance Object Storage, API compatible with Amazon S3 cloud storage service." \
      description="MinIO object storage is fundamentally different. Designed for performance and the S3 API, it is 100% open-source. MinIO is ideal for large, private cloud environments with stringent security requirements and delivers mission-critical availability across a diverse range of workloads."

ENV MINIO_ACCESS_KEY_FILE=access_key \
    MINIO_SECRET_KEY_FILE=secret_key \
    MINIO_ROOT_USER_FILE=access_key \
    MINIO_ROOT_PASSWORD_FILE=secret_key \
    MINIO_KMS_SECRET_KEY_FILE=kms_master_key \
    MINIO_UPDATE_MINISIGN_PUBKEY="RWTx5Zr1tiHQLwG9keckT0c45M3AGeHD6IvimQHpyRywVWGbP1aVSGav" \
    MINIO_CONFIG_ENV_FILE=config.env \
    MC_CONFIG_DIR=/tmp/.mc

# 拷贝官方镜像中的其他可执行文件
COPY --from=original_image /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=original_image /usr/bin/mc /usr/bin/mc
COPY --from=original_image /usr/bin/curl /usr/bin/curl
COPY --from=original_image /licenses/CREDITS /licenses/CREDITS
COPY --from=original_image /licenses/LICENSE /licenses/LICENSE
COPY --from=original_image /usr/bin/docker-entrypoint.sh /usr/bin/docker-entrypoint.sh

# 拷贝编译阶段输出的可执行文件
COPY --from=builder /go/src/minio/minio /usr/bin/minio

EXPOSE 9000
VOLUME ["/data"]

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh"]
CMD ["minio"]
