# Toolkit 工具包

## 准备工作: 安装 buildah 和 qemu
这两个工具包用于创建 docker image

```
sudo apt-get install buildah qemu-user-static
```

## buildah 命令的步骤
为了使容器支持 x86 和 ARM 两种 CPU 架构, 必须手动创建 manifest 清单
```
export VERSION="20240628"
export MANIFEST="localhost/minio:${VERSION}"

# 创建 manifest
buildah manifest create ${MANIFEST}
# buildah manifest inspect ${MANIFEST}

# 构建 linux/x86_64 架构的镜像
buildah build --manifest ${MANIFEST} \
    --build-arg RELEASE="${VERSION}" \
    --platform linux/amd64 \
    -t localhost/minio:${VERSION}_linux_amd64 \
    .
```
